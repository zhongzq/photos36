package Photos.app;

import Photos.controller.MainController;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 
 * 
 * @version javaSE-1.8
 * 
 */
public class Photos extends Application {


	@Override
	public void start(Stage primaryStage) throws Exception {
		MainController.start(primaryStage);
	}


	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void stop(){
        MainController.storeModelToFile();
	}
}