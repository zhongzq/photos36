package Photos.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;

public class Album implements Comparable<Album>, Serializable {

	/**
	 * This Album class is inside  Non-Admin User Subsystem which mainly defines a number of functions as follows: displaying
	 * all simple properties about album like name, # of photos, related date and so on.
	 * 
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	private String albumNameToKeep;

    private  SimpleStringProperty albumName;

    private  SimpleIntegerProperty photoNum;

    private  SimpleStringProperty startTime;

    private  SimpleStringProperty endTime;

    private List<Photo> photoList;

    private int curPhotoIdx;
    
    /**
     * @param albumName Name of the album
     */
    public Album(String albumName) {
		this.albumName 			= new SimpleStringProperty(albumName);
		this.photoNum 			= new SimpleIntegerProperty(0);
		this.startTime 			= new SimpleStringProperty("Not Availiable");
		this.endTime 			= new SimpleStringProperty("Not Availiable");
		this.photoList 			= new ArrayList<>();
		this.curPhotoIdx 	= -1;
	}
    /**
     * @return album's name 
     */
    public String getAlbumName() {
        return albumName.get();
    }

    /**
     * @param albumName album's name 
     */
    public void setAlbumName(String albumName) {
        this.albumName.set(albumName);
    }

    /**
     * @return Number of photos
     */
    public int getPhotoNum() {
        return photoNum.get();
    }

    /**
     * @param photoNum Number of photos
     */
    public void setPhotoNum(int photoNum) {
        this.photoNum.set(photoNum);
    }

    /**
     * @return the start  time of the picture be taken
     */
    public String getStartTime() {
        return startTime.get();
    }

    /**
     * @param startTime the start  time of the picture be taken
     */
    public void setStartTime(String startTime) {
        this.startTime.set(startTime);
    }

    /**
     * @return the end time of the picture be taken
     */
    public String getEndTime() {
        return endTime.get();
    }

    /**
     * @param endTime the end time of the picture be taken
     */
    public void setEndTime(String endTime) {
        this.endTime.set(endTime);
    }

    /**
     * @param isStore Clean up before store to file or after retrieved from file
     */
    public void doCleanUp(boolean isStore) {
		if (isStore) {
			albumNameToKeep = getAlbumName();
			albumName 	= null;
			photoNum 	= null;
			startTime 	= null;
			endTime 	= null;
		} else {
			albumName 	= new SimpleStringProperty(albumNameToKeep);
		    photoNum 	= new SimpleIntegerProperty();
		    startTime 	= new SimpleStringProperty();
		    endTime 	= new SimpleStringProperty();
		    albumNameToKeep = null;
		}
		for (Photo p: photoList) {
			p.doCleanUp(isStore);
		}
	}

    /**
     * @param albumName  name of the album	
     * @param photoLst 	list of photo
     */
    public Album(String albumName, List<Photo> photoLst) {
		this(albumName);
		for (Photo p : photoLst) {
		    Photo newOne = new Photo(p);
		    photoList.add(newOne);
	    }
	    if (photoList.size()>0) {
			curPhotoIdx = 0;
		}
	}

    /**
     * @param input Input index of current photo
     * @return index of current photo
     */
    public int setCurPhotoIdx(int input) {
		curPhotoIdx = input;
		return resetCurPhotoIdx();
	}

    /**
      * @return Index of current photo
     */
    private int resetCurPhotoIdx() {
		if (photoList.size()==0) {
		    curPhotoIdx = -1;
		} else {
			if (curPhotoIdx > photoList.size() - 1) {
				curPhotoIdx  = photoList.size() - 1;
			} else if (curPhotoIdx < 0) {
				curPhotoIdx = 0;
			}
		}
		return curPhotoIdx;
	}

    /**
     * @return Index of current photo
     */
    public int getCurPhotoIdx() {
		return 	resetCurPhotoIdx();
	}


    /**
     * @param photo Input photo to be added
     */
    public void addPhoto(Photo photo) {
    	photoList.add(photo);
    	curPhotoIdx = photoList.size() - 1;
	}


    /**
     * @param index Index of photo
     * @return Photo from index
     */
    public Photo getPhotoFromAlbum(int index) {
    	if (index < photoList.size() && index>=0) {
    		return photoList.get(index);
    	}
    	return null;
    }

    /**
     * @param photo Input photo
     * @return Index of the photo
     */
    public int getPhotoIdx(Photo photo) {
    	for (int i = 0; i < photoList.size(); i++) {
    		if (photoList.get(i) == photo) return i;
    	}
    	return -1;
    }


    /**
     * @param photo Input photo
     * @return the index of photo deleted
     */
    public int deletePhoto(Photo photo) {
    	int index = -1;
    	for (int i=0; i<photoList.size(); i++) {
    		if (photo==photoList.get(i)) {
    			index = i;
    			photoList.remove(index);
    			break;
    		}
    	}
    	return index;
    }


    /**
     * @param photoAt Photo: the location of the new photo will be added
     * @param photoAdd Photo  adding the specified photo 
     * @return Index of the added photo 
     */
    public int addPhotoToAlbum(Photo photoAt, Photo photoAdd) {
    	int index;
        if (photoAt != null) {
        	for (int i = 0; i < photoList.size(); i++) {
        		if (photoList.get(i) == photoAt) return index = i;
        	}
        	index =  -1;
        } else index = photoList.size();
		photoList.add(index, photoAdd);
    	return index;
    }


    /**
     * @return List of photos
     */
    public List<Photo> getPhotoList() {
    	return photoList;
    }

    /**
     * @return Size of list of photos
     */
    public int getSize() {
    	return photoList.size();
    }


    /**
     * @return Current photo
     */
    public Photo getCurPhoto() {
    	resetCurPhotoIdx();
        return curPhotoIdx >= 0 ? photoList.get(curPhotoIdx) : null;
    }


    /**
     * @param album Album
     */
    @Override
	public int compareTo(Album album) {
		return getAlbumName().compareToIgnoreCase(album.getAlbumName());
	}


    /**
     * @return Album name
     */
    @Override
	public String toString() {
		return getAlbumName();
	}


    /**
     * @param tags search photos by specified photo tags
     * @return List of photos showing search result 
     */
    public List<Photo> doSearchTag(TagOperations tags) {
    	List<Photo> res = new ArrayList<Photo>();
    	for (Photo p: photoList) {
    		Set<String> set = new HashSet<String>();
    		for (Tag t: tags.getTags()) {
    			set.add(t.toString());
    		}
    		for (Tag t: p.getTags()) {
    			if (set.contains(t.toString())) {
    				set.remove(t.toString());
    			}
    			if (set.size() == 0) break;
    		}
    		if (set.size() == 0) res.add(p);
    	}
    	return res;
	}


    /**
     * @param dates the date chosen by user to make search 
     * @return List of photos in the given range of dates
     */
    public List<Photo> doSearchDate(TimeOperations dates) {
    	List<Photo> res = new ArrayList<Photo>();
    	for (Photo p: photoList) {
    		if (p.withinRange(dates)) {
    			res.add(p);
    		}
    	}
    	return res;
	}

    public void setCounterDatetime() {
		if (photoList.size()==0) {
			setPhotoNum(0);
		    setStartTime("Not Available");
		    setEndTime("Not Available");
		}
		else {
			boolean start = true;
			int count 	= 0;
			long min	= 0;
			long max	= 0;
			for (Photo p: photoList) {
				if (start) {
					count = 1;
					min	= p.getDateOfPhoto();
					max	= p.getDateOfPhoto();
					start = false;
				} else {
					count++;
					long pd = p.getDateOfPhoto();
					if (pd > max) {
						max = pd;
					}
					if (pd < min) {
						min = pd;
					}
				}
			}
			setPhotoNum(count);
		    setStartTime(Photo.convertLastModifiedTime(min));
		    setEndTime(Photo.convertLastModifiedTime(max));
		}
	}
}