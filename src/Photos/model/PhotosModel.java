package Photos.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.BiPredicate;

public class PhotosModel implements Serializable {

	/**
	 * This class mainly defines a number of functions toward the Admin Subsystem, the main functions are as follows: 
	 * initialize a list of user account of Photo model and some operations of the list like cleanup, and create or delete the user account
	 * 
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	private ObservableList<User> userList;

    private ArrayList<User> listOfUsersToKeep;

    private User curUser;

    
    public PhotosModel() {
        userList = FXCollections.observableArrayList();
        listOfUsersToKeep = null;
        curUser = null;
    }

    /**
     * @param isStore Clean up before store to file or after retrieve from file
     */
    public void doCleanUp(boolean isStore) {
        if (isStore) {
            listOfUsersToKeep = new ArrayList<>(userList);
            userList = null;

            for (User u : listOfUsersToKeep) {
                u.doCleanUp(true);
            }
        } else {
            userList = FXCollections.observableList(listOfUsersToKeep);
            listOfUsersToKeep = null;
            
            for (User u : userList) {
                u.doCleanUp(false);
            }
        }
    }


    /**
     * @return List of user account
     */
    public ObservableList<User> getUserList() {
        return userList;
    }


    /**
     * @return current User 
     */
    public User getCurUser() {
        return curUser;
    }


    /**
     * @param curUser User 
     */
    public void setCurUser(User curUser) {
        this.curUser = curUser;
    }


    /**
     * @param userName adding username to the list 
     */
    public void addUser(String userName) {
        User user = new User(userName);
        ListIterator<User> iterator = userList.listIterator();

        while (true) {
            if (!iterator.hasNext()) {
                iterator.add(user);
                return ;
            }
            User element = iterator.next();
            if (element.compareTo(user) == 0) {
            	return ;
            }
            if (element.compareTo(user) > 0) {
                iterator.previous();
                iterator.add(user);
                return ;
            }
        }
    }
 
    /** 
     * @param i Index of user in the list
     */
    public void deleteUser(int i) {
        if (i>=0 && i<userList.size()) {
            if (!userList.get(i).getUsername().equalsIgnoreCase("admin")) {
                userList.remove(i);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Admin cannot be deleted.");
                alert.showAndWait();
            }
        }
    }

    /**
     * @param userName Username
     * @return User printing the user account in the user list
     */
    public User getUser(String userName) {
    	for (User user: userList) {
    		if (user.getUsername().equalsIgnoreCase(userName)) return user;
    	}
    	return null;
    }
}