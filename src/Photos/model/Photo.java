package Photos.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import java.util.function.BiPredicate;

public class Photo implements Serializable {
	
	/**
	 * This photo class is a album management mainly defines a number of functions as follows: displaying
	 * all simple properties about photo like name, photo style, caption, date, tags and so on.
	 * 
	
	 */
	
	private static final long serialVersionUID = 1L;

	private String fileName;

    private String thumbnail;

    private String caption;

    private long dateOfPhoto;

    private ObservableList<Tag> listTags;

    private ArrayList<Tag> listOfTagsToKeep;

    /**
     * @param _fileName Photo's name
     * @param _thumbnail Thumbnail created from photo
     * @param _caption   photo'Caption
     * @param _dateOfPhoto Date of photo
     */
    private Photo(String _fileName, String _thumbnail, String _caption, long _dateOfPhoto) {
        fileName 		= _fileName;
        thumbnail		= _thumbnail;
        caption 		= _caption;
        dateOfPhoto		= _dateOfPhoto;

        listTags 		= FXCollections.observableArrayList();
        listOfTagsToKeep = null;
    }

    static {
        new File("thumbnails").mkdir();
    }

    /**
     * @param input Input thumbname
     * @return Thumbnail file name
     */
    public static String getThumbnailFileName(String input) {
        return "thumbnails/" + input + ".png";
    }

    /**
     * @param filename photo name
     * @param file Photo file
     * @return New photo created
     */
    public static Photo createPhoto(String filename, File file) {
        if (file==null) {
            file = new File(filename);
        }
        long lastModified = file.lastModified();
        String shortFileName = file.getName();
        int pos = shortFileName.indexOf('.');
        if (pos > 0) {
            shortFileName = shortFileName.substring(0, pos);
        }
        String thumbnail = String.valueOf(UUID.randomUUID());
        boolean flag = createThumbNail(filename, getThumbnailFileName(thumbnail), 100, 100, "png");
        if (flag) {
            return new Photo(filename, thumbnail, shortFileName, lastModified);
        }
        return null;
    }


    /**
     * @param photoFileName  origin photo'name
     * @param thumbnailFileName File name of thumbnail
     * @param width size of thumbnail
     * @param height  
     * @param thumbnailFormat 
     * @return True checking if the thumbnail is created 
     */
    private static boolean createThumbNail(String photoFileName, String thumbnailFileName, int width, int height, String thumbnailFormat) {
        boolean flag = true;
        Image image = null;
        try {
            image = new Image(new FileInputStream(photoFileName), width, height, true, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (image!=null) {
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
            try {
                FileOutputStream fos = new FileOutputStream(thumbnailFileName);
                ImageIO.write(bufferedImage, thumbnailFormat, fos);
                fos.close();
            } catch (IOException e) {
                flag = false;
                e.printStackTrace();
            }
        } else {
            flag = false;
        }
        return flag;
    }

    /**
     * @param isStore Clean up before store to file or after retrieved from file
     */
    public void doCleanUp(boolean isStore) {
        if (isStore) {
            listOfTagsToKeep = new ArrayList<>(listTags);
            listTags = null;
        } else {
            listTags = FXCollections.observableList(listOfTagsToKeep);
            listOfTagsToKeep = null;
        }
    }



    /**
     * @return List of tags
     */
    public ObservableList<Tag> getTags() {
        return listTags;
    }

    /**
     * @param photo Copying a photo
     */
    public Photo(Photo photo) {
        this.fileName = photo.fileName;
        this.thumbnail = String.valueOf(UUID.randomUUID());
        this.caption = photo.caption;
        this.dateOfPhoto = photo.dateOfPhoto;
        listTags 		= FXCollections.observableArrayList();
        for (Tag t: photo.listTags) {
            listTags.add(new Tag(t));
        }
        try {
            Files.copy(new File(photo.getThumbnailFileName()).toPath(), new File(getThumbnailFileName()).toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return File name of thumbnail
     */
    private String getThumbnailFileName() {
        return getThumbnailFileName(thumbnail);
    }


    /**
     * @return File name of photo
     */
    public String getFileName() {
        return fileName;
    }


    /**
     * @param fileName photo'name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    /**
     * @return Caption of photo
     */
    public String getCaption() {
        return caption;
    }


    /**
     * @param caption Caption of photo
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }


    /**
     * @return the Date of photo 
     */
    public long getDateOfPhoto() {
        return dateOfPhoto;
    }

    /**
     * @return the particular time of photo 
     */
    public String getDateOfPhotoString() {
        return convertLastModifiedTime(dateOfPhoto);
    }


    /**
     * @param tagName name of the tag
     * @param tagValue  _tagvalue
     * @return True checking if the tag is added
     */
    public boolean addTag(String tagName, String tagValue) {
        Tag t = new Tag(tagName, tagValue);
        ListIterator<Tag> iterator = listTags.listIterator();
        
        while(true) {
        	if (!iterator.hasNext()) {
        		iterator.add(t);
                return true;
        	}
        	Tag element = iterator.next();
            if (element.compareTo(t) == 0) {
                return false;
            }
            if (element.compareTo(t) > 0) {
            	iterator.previous();
            	iterator.add(t);
                return true;
            }
        }
    }

    public Tag deleteTag(String tagName, String tagValue) {
        Tag t = new Tag(tagName, tagValue);
        ListIterator<Tag> iterator = listTags.listIterator();

        while (iterator.hasNext()) {
        	Tag tag = iterator.next();
        	if (tag.compareTo(t) == 0) {
        		iterator.remove();
        		return t;
        	}
        }
        return null;        
    }

    /**
     * @param i the Index of tag in list
     */
    public void deleteTag(int i) {
        if (i>=0 && i<listTags.size()) {
            listTags.remove(i);
        }
    }

    /**
     * @param handler Handler for mouse events
     * @param style Style of Image View
     * @return Thumbnail Image View
     */
    public BorderPane getThumbnailImageView(EventHandler<MouseEvent> handler, String style) {
        Image image = new Image("File:"+getThumbnailFileName(), 0, 0, false, false);
        ImageView view = new ImageView(image);
        view.setFitWidth(100);
        view.setFitHeight(100);
        view.setOnMouseClicked(handler);
        view.setUserData(this);
        Photo thisPhoto = this;
        TextField textfield = new TextField(getCaption());
        textfield.setPrefWidth(100);
        textfield.setOnAction(event -> {
            TextField textField = (TextField) event.getSource();
            String temp = textField.getText().trim();
            if (temp.length()==0) {
                textField.setText(thisPhoto.getCaption());
            } else {
                thisPhoto.setCaption(temp);
            }
        });
        VBox vbox = new VBox(4); 
        vbox.getChildren().addAll(view, textfield, new Label(getDateOfPhotoString()));
        BorderPane viewWrapper = new BorderPane(vbox);
        viewWrapper.setStyle(style);
        return viewWrapper;
    }

    /**
     * @param handler Handler for mouse events
     * @return Node for Image to render
     */
    public Node getImageComponent(EventHandler<MouseEvent> handler) {
        ImageView image;
        Image imageR = null;
        try {
            imageR = new Image(new FileInputStream(getFileName()));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        image = new ImageView();
        image.setFitWidth(500);
        image.setFitHeight(400);
        image.setPreserveRatio(true);
        image.setSmooth(true);
        image.setImage(imageR);
        image.setOnMouseClicked(handler);
        VBox vbox = new VBox(2); 	
        vbox.getChildren().addAll(new Label(getDateOfPhotoString()), image);
        return vbox;
    }


    /**
     * @param dates Range of dates
     * @return True if within range of dates
     */
    public boolean withinRange(TimeOperations dates) {
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDate startDate 	= dates.getStartDate();					//starting of startdate
        LocalDate endDate	 	= dates.getEndDate().plusDays(1);		//end of enddate
        long start 	= startDate.atStartOfDay(zoneId).toEpochSecond();
        long end 	= endDate.atStartOfDay(zoneId).toEpochSecond();
        Date dateOfPhoto = new Date(this.dateOfPhoto);
        LocalDateTime date = dateOfPhoto.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        long toCheck = date.atZone(zoneId).toEpochSecond();
        return toCheck >= start && toCheck < end;
    }


    /**
     * @param time Input time
     * @return Last modified time converted to String
     */
    public static String convertLastModifiedTime(long time) {
        LocalDateTime datetime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return datetime.format(formatter);
    }
}