package Photos.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

public class TagOperations implements Serializable {


	/**
	 * Defining class for TagOperations
	 * 

	 */
	
	private static final long serialVersionUID = 1L;

    private ObservableList<Tag> listTags;

    private ArrayList<Tag> listOfTagsToKeep;

    public TagOperations() {
	    listTags = FXCollections.observableArrayList();
	}
    
	/**
     * @param isStore Clean up before store to file or after retrieve from file
     */
    public void doCleanUp(boolean isStore) {
		if (isStore) {
			listOfTagsToKeep = new ArrayList<>(listTags);
			listTags 		= null;
		} else {
			listTags 		= FXCollections.observableList(listOfTagsToKeep);
			listOfTagsToKeep = null;
		}
	}

    /**
     * @return List of tags
     */
    public ObservableList<Tag> getTags() {
		return listTags;
	}


    /**
     * @param tagName Tag name
     * @param tagValue Tag value
	 * @return True if tag is added
     */
    public boolean addTag(String tagName, String tagValue) {
    	Tag t = new Tag(tagName, tagValue);
    	ListIterator<Tag> itr = listTags.listIterator();
        while (true) {
            if (!itr.hasNext()) {
                itr.add(t);
                return true;
            }
            Tag elementInList = itr.next();
            if (elementInList.compareTo(t) == 0) {
                return false;
            }
            if (elementInList.compareTo(t) > 0) {
                itr.previous();
                itr.add(t);
                return true;
            }
        }
    }
}