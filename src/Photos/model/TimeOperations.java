package Photos.model;

import java.io.Serializable;
import java.time.LocalDate;

public class TimeOperations implements Serializable {

	/**
	 * Defining class for TimeOperations
	 * 
	
	 */
	
	private static final long serialVersionUID = 1L;

	private LocalDate startDate;

    private LocalDate endDate;

	
	public TimeOperations() {
		this.startDate = null;
		this.endDate = null;
	}

    /**
     * @return start date
     */
    public LocalDate getStartDate() {
		return startDate;
	}


    /**
     * @param startDate Start date
     */
    public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}


    /**
     * @return End date
     */
    public LocalDate getEndDate() {
		return endDate;
	}


    /**
     * @param endDate LocalDate
     */
    public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
}