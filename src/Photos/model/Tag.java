package Photos.model;

import java.io.Serializable;

public class Tag implements Comparable<Tag>, Serializable {


	private static final long serialVersionUID = 1L;

	private String tagName;

    private String tagValue;

    
    public Tag(String _tagName, String _tagValue) {
        tagName = _tagName;
        tagValue = _tagValue;
    }


    /**
     * @param t Tag from user input
     */
    public Tag(Tag t) {
        tagName = t.tagName;
        tagValue = t.tagValue;
    }


    /**
     * @return tags of picture
     */
    public String toString() {
    	return tagName + "=" + tagValue;
    }

    /**
     * @param tag Tag from user input
     * @return Checks if the tags are equal to each other
     */
    @Override
    public int compareTo(Tag tag) {
    	if (tagName.compareToIgnoreCase(tag.tagName) == 0) {
    		return tagValue.compareToIgnoreCase(tag.tagValue);
    	} else {
    		return tagName.compareToIgnoreCase(tag.tagName);
    	}
    }
}