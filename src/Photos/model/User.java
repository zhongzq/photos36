package Photos.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;

public class User implements Comparable<User>, Serializable {

	/**
	 * This class mainly defines a number of functions toward the userSubsytem, the main functions are as follows: 
	 * initialize the list of Albums and various operations of Album like cleanup, add, update and search photos by 
	 * date or tags.
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	private String username;

    private ObservableList<Album> listAlbums;

    private ArrayList<Album> listOfAlbumsToKeep;

    private Album curAlbum;

    private TagOperations tags;

    private TimeOperations dates;

    public User(String _username) {
		username = _username;
		listAlbums = FXCollections.observableArrayList();
		listOfAlbumsToKeep = null;
		curAlbum = null;
		tags = new TagOperations();
		dates = new TimeOperations();
	}
    
    /**
     * @param isStore Clean up before store to file or after retrieve from file
     */
    public void doCleanUp(boolean isStore) {
		if (isStore) {
			listOfAlbumsToKeep = new ArrayList<>(listAlbums);
			listAlbums = null;
			for (Album a : listOfAlbumsToKeep) {
				a.doCleanUp(true);
			}
		} else {
			listAlbums = FXCollections.observableList(listOfAlbumsToKeep);
			listOfAlbumsToKeep = null;
			for (Album a : listAlbums) {
				a.doCleanUp(false);
			}
		}
		tags.doCleanUp(isStore);
	}

    /**
     * Creating a new Album and add it to the listAlbums
     * @param username Username
     * @return a new adding Album 
     */
    public Album addAlbum(String username) {
		Album album = new Album(username);
		
		boolean flag = false;
		for (Album a: listAlbums) {
			if (a.getAlbumName().equalsIgnoreCase(album.getAlbumName())) flag = true;
		}
		if (!flag) {
			listAlbums.add(album);
			return album;
		} else {
			return null;
		}
	}

    /** Adding search result to the bottom of the Album list
     * @param album Album. 
     */
    public void addOrOverwriteAlbum(Album album) {
    	for (int i = 0; i < listAlbums.size(); i++) {
    		if (listAlbums.get(i).getAlbumName().equalsIgnoreCase(album.getAlbumName())) {
    			listAlbums.remove(i);
    		}
    	}
    	listAlbums.add(album);
	}

    /**
     * @param index Index of Album
     * @param albumName new adding albumName
     */
    public void updateAlbumName(int index, String albumName) {
		if (index >= 0 && index < listAlbums.size()) {
			boolean flag = false;
			for (int i = 0; i < listAlbums.size(); i++) {
	    		if (listAlbums.get(i).getAlbumName().equalsIgnoreCase(albumName)) {
	    			flag = true;
	    		}
	    	}
			if (!flag) listAlbums.get(index).setAlbumName(albumName);
		}
	}


    /**
     * @return Current album
     */
    public Album getCurrentAlbum() {
		return curAlbum;
	}

    /**
     * @param currentAlbum Album
     */
    public void setCurrentAlbum(Album currentAlbum) {
        this.curAlbum = currentAlbum;
    }

    /**
     * @return Tags of searching
     */
    public TagOperations getTags() {
        return tags;
    }


    /**
     * @param index Index of tags in the list
     */
    public void deleteTag(int index) {
		ObservableList<Tag> tagList = tags.getTags();
		if (tagList.size() > 0 && index >= 0 && index < tagList.size()) {
			tagList.remove(index);
		}
	}

    /**
     * @return Range of dates
     */
    public TimeOperations getDates() {
        return dates;
    }

    /**
     * @return List of albums
     */
    public ObservableList<Album> getListAlbums() {
		return listAlbums;
	}


    /**
     * @return username
     */
    public String getUsername() {
		return username;
	}
	
    /**
     * Searching requested album by name and return the result in the list
     * @param albumName Album 
     * @return Album 
     */
    public Album getAlbum(String albumName) {
    	for (Album a: listAlbums) {
    		if (a.getAlbumName().equalsIgnoreCase(albumName)) return a;
    	}
    	return null;
	}

    /**
     * Adding/copying photo to another specified Album
     * @param photo Photo (source)
     * @param albumName Album (destination)
     */
    public void copyPhoto(Photo photo, String albumName) {
		Photo newPhoto = new Photo(photo);
		Album album = getAlbum(albumName);
		album.addPhoto(newPhoto);
	}

    /**
     * transferring photo to another specified Album
     * @param photo Photo 
     * @param albumName Album 
     */
    public void movePhoto(Photo photo, String albumName) {
		Album album = getAlbum(albumName);
		curAlbum.deletePhoto(photo);
		album.addPhoto(photo);
	}


    /**
     * @return username
     */
    @Override
	public String toString() {
		return username;
	}


    /**
     * @param user username
     * @return Checks whether the user input is equal to the username or not
     */
    @Override
	public int compareTo(User user) {
		return username.compareToIgnoreCase(user.username);
	}


    /**
     * @return List of photos with specified tags
     */
    public List<Photo> searchByTag() {
		List<Photo> list = new ArrayList<>();
		for (Album a : listAlbums) {
            if (!a.getAlbumName().equalsIgnoreCase("Search Result(Date)") && !a.getAlbumName()
                    .equalsIgnoreCase("Search Result(Tag)")) {
                list.addAll(a.doSearchTag(tags));
            }
        }
		return list;
	}


    /**
     * @return List of photos within the range of searching dates
     */
    public List<Photo> searchByDate() {
		List<Photo> list = new ArrayList<>();
		for (Album a : listAlbums) {
            if (!a.getAlbumName().equalsIgnoreCase("Search Result(Date)") && !a.getAlbumName()
                    .equalsIgnoreCase("Search Result(Tag)")) {
                list.addAll(a.doSearchDate(dates));
            }
        }
		return list;
	}
}
