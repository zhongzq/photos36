package Photos.controller;

import Photos.model.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class AlbumController extends MainController implements EventHandler<MouseEvent>, ChangeListener<Tab>, IController {

    static String white = "-fx-border-color: white;\n" + "-fx-border-style: solid;\n" + "-fx-border-width: 3;\n";
 
    static String blue = "-fx-border-color: blue;\n" + "-fx-border-style: solid;\n" + "-fx-border-width: 3;\n";


    @FXML TabPane tab;
    @FXML TilePane tile;

    @FXML ListView<Tag> listTag;

    @FXML Pagination pagination;

	@FXML TextField tagName;
    @FXML TextField tagValue;

    ObservableList<Node> list;

    ContextMenu cm = new ContextMenu();

    Menu cmCopy = new Menu("Copy to");
    Menu cmMove = new Menu("Move to");
    
	ContextMenu cm_tile = new ContextMenu();


    /**
     * Initial with album to be shown
     */
    @Override
	public void init() {
    	tab.getSelectionModel().select(0);
    	User user = getModel().getCurUser();
    	Album album = user.getCurrentAlbum();
    	list.clear();
    	List<Photo> photoList = album.getPhotoList();
        for (Photo onePhoto : photoList) {
            BorderPane viewWrapper = onePhoto.getThumbnailImageView(this, white);
            list.add(viewWrapper);
        }
    	int index = album.getCurPhotoIdx();
        setupCurrentPhoto(index, true);
	}

    public AlbumController() {
    	AlbumController controller = this;
    	MenuItem cmItemAdd = new MenuItem("Add");
    	cmItemAdd.setOnAction(e -> {
            Photo onePhoto = (Photo)cm.getUserData();
            User user = getModel().getCurUser();
            Album album = user.getCurrentAlbum();
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open File");
            File file = chooser.showOpenDialog(new Stage());
            if (file!=null) {
                Photo photo = Photo.createPhoto(file.getAbsolutePath(), file);
                int oldcurrIndex = album.getCurPhotoIdx();
                int index = album.addPhotoToAlbum(onePhoto, photo);
				BorderPane viewWrapper = null;
				if (photo != null) {
					viewWrapper = photo.getThumbnailImageView(controller, white);
				}
                list.add(index, viewWrapper);
                if (index<=oldcurrIndex) {
                	BorderPane vw = (BorderPane)list.get(oldcurrIndex+1);
                    vw.setStyle(white);
                }
                setupCurrentPhoto(index, true);
            }
        });
    	cm.getItems().add(cmItemAdd);
    	cm.getItems().add(cmCopy);
    	cm.getItems().add(cmMove);
    	MenuItem cmItemDelete = new MenuItem("Delete");
    	cmItemDelete.setOnAction(e -> {
            Photo onePhoto = (Photo)cm.getUserData();
            User user = getModel().getCurUser();
            Album album = user.getCurrentAlbum();
            int index = album.deletePhoto(onePhoto);
            list.remove(index);
            int indexCurr = album.getCurPhotoIdx();
            setupCurrentPhoto(indexCurr, true);
        });
    	cm.getItems().add(cmItemDelete);
	}


    /**
     * Initialize album controller
     */
    @FXML
    public void initialize() {
    	list = tile.getChildren();
    	tile.setOnMouseClicked(this);
        tab.getSelectionModel().selectedItemProperty().addListener(this);
        tab.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        pagination.setPageFactory(pageIndex -> {
            User user = getModel().getCurUser();
            Album album = user.getCurrentAlbum();
            if (album.getSize() > 0) {
                AlbumController.this.setupCurrentPhoto(pageIndex, false);
                Photo currPhoto = album.getCurPhoto();
                return currPhoto.getImageComponent(arg0 -> tab.getSelectionModel().select(0));
            } else {
                return null;
            }
        });
    }

    
    /**
     * @param photo Input photo
     */
    public void setupCurrentPhoto(Photo photo) {
    	User user = getModel().getCurUser();
    	Album album = user.getCurrentAlbum();
    	int index = album.getPhotoIdx(photo);
    	setupCurrentPhoto(index, true);
    }

    /**
     * @param newIndex Index for current photo in the album
     * @param isFromAlbum if true initialize pagination too.
     */
    public void setupCurrentPhoto(int newIndex, boolean isFromAlbum) {
    	User user = getModel().getCurUser();
    	Album album = user.getCurrentAlbum();
		int oldIndex = album.getCurPhotoIdx();
		newIndex = album.setCurPhotoIdx(newIndex);
		Photo newPhoto = album.getCurPhoto();
    	if (newIndex > -1) {
	    	if (list.size()>0) {
	    		if (oldIndex==-1) {
			        BorderPane currViewWrapper = (BorderPane)list.get(newIndex);
			        currViewWrapper.setStyle(blue);
	    		} else if (oldIndex!=newIndex) {
			        BorderPane oldViewWrapper = (BorderPane)list.get(oldIndex);
			        BorderPane currViewWrapper = (BorderPane)list.get(newIndex);
			        oldViewWrapper.setStyle(white);
			        currViewWrapper.setStyle(blue);
		    	} else {
			        BorderPane currViewWrapper = (BorderPane)list.get(newIndex);
			        currViewWrapper.setStyle(blue);
		    	}
	    	}
	        listTag.setItems(newPhoto.getTags());
	        if (newPhoto.getTags().size() > 0) {
	        	listTag.getSelectionModel().select(0);
	        }
	        if (isFromAlbum) {
	        	pagination.setPageCount(album.getSize());
	        	pagination.setCurrentPageIndex(newIndex);
	        }
    	}
    }


    /**
     * @param event Event for mouse clicks
     */
    @Override
	public void handle(MouseEvent event) {
		Object obj = event.getSource();
		if (obj instanceof TilePane) {
	        if (event.getButton() == MouseButton.SECONDARY) {
	        	cm_tile.show(tile, event.getScreenX(), event.getScreenY());
	        }
	        event.consume();
		} else if (obj instanceof ImageView) {
			ImageView view = (ImageView)obj;
	        if (event.getButton().equals(MouseButton.PRIMARY)) {
	            if (event.getClickCount() == 1) {
	            	Object obj1 = view.getUserData();
	            	Photo onePhoto = (Photo)obj1;
	            	setupCurrentPhoto(onePhoto);
	            } else {
	            	Object obj1 = view.getUserData();
	            	Photo onePhoto = (Photo)obj1;
	            	setupCurrentPhoto(onePhoto);
	            	tab.getSelectionModel().select(1);
	            }
	        } else if (event.getButton() == MouseButton.SECONDARY) {
            	Object obj1 = view.getUserData();
            	Photo onePhoto = (Photo)obj1;
            	PhotosModel model = getModel();
            	User user = model.getCurUser();
            	ObservableList<Album> albums =user.getListAlbums();
            	Album album = user.getCurrentAlbum();
            	List<Album> albumList = new ArrayList<Album>();
            	for (Album a: albums) {
            		if (!a.getAlbumName().equalsIgnoreCase("Search Result(Date)") && 
            				!a.getAlbumName().equalsIgnoreCase("Search Result(Tag)") && 
            				!a.getAlbumName().equalsIgnoreCase(album.getAlbumName())) {
            			albumList.add(a);
            		}
            	}
                
            	cmCopy.getItems().clear();
            	for (Album a : albumList) {
                	MenuItem cmItemAlbum = new MenuItem(a.getAlbumName());
                	cmItemAlbum.setOnAction(e -> {
                        MenuItem mi =  (MenuItem)e.getSource();
                        User user1 = getModel().getCurUser();
                        Photo onePhoto1 = (Photo)cm.getUserData();
                        String albumTarget = mi.getText();
                        user1.copyPhoto(onePhoto1, albumTarget);
                    });
                	cmCopy.getItems().add(cmItemAlbum);
            	}

            	cmMove.getItems().clear();
            	for (Album a : albumList) {
                	MenuItem cmItemAlbum = new MenuItem(a.getAlbumName());
                	cmItemAlbum.setOnAction(e -> {
                        MenuItem mi =  (MenuItem)e.getSource();
                        Photo onePhoto12 = (Photo)cm.getUserData();
                        String albumTarget = mi.getText();
                        User user12 = getModel().getCurUser();
                        Album album1 = user12.getCurrentAlbum();
                        int index = album1.getPhotoIdx(onePhoto12);
                        list.remove(index);
                        user12.movePhoto(onePhoto12, albumTarget);
                        int iCurr = album1.getCurPhotoIdx();
                        setupCurrentPhoto(iCurr, true);
                    });
                	cmMove.getItems().add(cmItemAlbum);
            	}
	        	cm.setUserData(onePhoto);
	        	cm.show(view, event.getScreenX(), event.getScreenY());
	        }
	        event.consume();
		}
	}

    public void logOff() {
	    gotoLoginFromAlbum();
	}

    public void exit() {
		Platform.exit();
	}


    /**
     * Event handle of tab switching
     */
	@Override
	public void changed(ObservableValue<? extends Tab> arg0, Tab arg1, Tab arg2) {
		if (arg2.getText().equals("Photo")) {
	    	User user = getModel().getCurUser();
	    	Album album = user.getCurrentAlbum();
        	Photo currPhoto = album.getCurPhoto();

        	if (currPhoto!=null) {
        		pagination.setVisible(true);
	        	int index = album.getPhotoIdx(currPhoto);
				pagination.setPageCount(album.getSize());
				pagination.setCurrentPageIndex(index);
        	} else {
        		pagination.setVisible(false);
        	}
		} 
	}

    public void deleteTag() {
    	User user = getModel().getCurUser();
    	Album album = user.getCurrentAlbum();
    	Photo currPhoto = album.getCurPhoto();
    	int index = listTag.getSelectionModel().getSelectedIndex();
    	currPhoto.deleteTag(index);
    	listTag.refresh();
	}

    public void addTag() {
    	String name = tagName.getText().trim();
    	String value = tagValue.getText().trim();
    	if (name.length() > 0 && value.length() > 0) {
	    	User user = getModel().getCurUser();
	    	Album album = user.getCurrentAlbum();
	    	Photo currPhoto = album.getCurPhoto();
	    	boolean result = currPhoto.addTag(name, value);
			if (result) {
                listTag.refresh();
                tagName.setText("");
                tagValue.setText("");
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Duplicate Name and Value Pair. Try again.");
                alert.showAndWait();
            }
    	} else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Name and Value Fields are empty. Enter again");
            alert.showAndWait();
    	}
    	
    }
    
    public void addNewPhoto() {
	    User user = getModel().getCurUser();
	    Album album = user.getCurrentAlbum();
	    FileChooser chooser = new FileChooser();
	    chooser.setTitle("Open Photo File");
	    File file = chooser.showOpenDialog(new Stage());
	    if (file!=null) {
	        Photo photo = Photo.createPhoto(file.getAbsolutePath(), file);
	        int index = album.addPhotoToAlbum(null, photo);
	        BorderPane viewWrapper = null;
	        if (photo != null) {
	            viewWrapper = photo.getThumbnailImageView(this, white);
	        }
	        list.add(index, viewWrapper);
	        setupCurrentPhoto(index, true);
	    }
    }

    public void returnToList() {
    	gotoUserFromAlbum();
    }

}