package Photos.controller;

import Photos.model.Album;
import Photos.model.Photo;
import Photos.model.PhotosModel;
import Photos.model.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class MainController {

	public static Stage loginStage = null;

    public static Stage albumStage = null;
    
    public static Scene sceneLogin = null;

    public static Scene sceneAdmin = null;

    public static Scene sceneUser = null;

    public static Scene sceneAlbum = null;

    public static IController controllerLogin = null;

    public static IController controllerAdmin = null;

    public static IController controllerUser = null;

    public static IController controllerAlbum = null;

    private static PhotosModel model;
   
    
    /**
     * @param fml popping up a helping instruction window 
     */
    public static void Help(String fml) {
    	Parent root;
		try {
			root = FXMLLoader.load(MainController.class.getResource(fml));
	    	Stage window = new Stage();
	    	window.initModality(Modality.APPLICATION_MODAL);
			Scene scene = new Scene(root);
			window.setScene(scene);
			window.setTitle("Help");
			window.setResizable(false);
			window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }


    /**
     * @param primaryStage Primary stage of application
     * @throws Exception In case stage does not start
     */
    public static void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainController.class.getResource("/Photos/view/Login.fxml"));
		Parent root = loader.load();
		sceneLogin = new Scene(root);
		controllerLogin = loader.getController();
	
		FXMLLoader loader1 = new FXMLLoader();
		loader1.setLocation(MainController.class.getResource("/Photos/view/Admin.fxml"));
		Parent root1 = loader1.load();
		sceneAdmin = new Scene(root1);
		controllerAdmin = loader1.getController();
	
		FXMLLoader loader2 = new FXMLLoader();
		loader2.setLocation(MainController.class.getResource("/Photos/view/userSubsystem.fxml"));
		Parent root2 = loader2.load();
		sceneUser = new Scene(root2);
		controllerUser = loader2.getController();
	
		FXMLLoader loader3 = new FXMLLoader();
		loader3.setLocation(MainController.class.getResource("/Photos/view/albumManagement.fxml"));
		Parent root3 = loader3.load();
		sceneAlbum = new Scene(root3);
		controllerAlbum = loader3.getController();
	
		primaryStage.setTitle("Not yet");
		primaryStage.setResizable(false);
		loginStage = primaryStage;
		albumStage = new Stage();
		primaryStage.setTitle("Not yet");
		albumStage.setResizable(false);
		gotoLogin();
	}

    /**
     * @return Model. Retrieve model from file. If could not create new model
     */
    public static PhotosModel getModel() {
        if (model == null) {
            try {
                FileInputStream fileIn = new FileInputStream("photo.dat");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                model = (PhotosModel)in.readObject();
                model.doCleanUp(false);
                in.close();
                fileIn.close();
            } catch(IOException | ClassNotFoundException i) {
                model = null;
            }

            if (model==null) {
                model = new PhotosModel();
                model.addUser("admin");
                model.addUser("stock");
                model.setCurUser(model.getUser("stock"));
                User user = model.getCurUser();
                
                Album album1 = user.addAlbum("cats");
                album1.addPhoto(Photo.createPhoto("stockphotos/cat1.jpg", null));
                album1.addPhoto(Photo.createPhoto("stockphotos/cat2.jpg", null));
                album1.addPhoto(Photo.createPhoto("stockphotos/cat3.jpg", null));
                album1.addPhoto(Photo.createPhoto("stockphotos/cat4.jpg", null));
                album1.addPhoto(Photo.createPhoto("stockphotos/cat5.jpg", null));
                album1.addPhoto(Photo.createPhoto("stockphotos/cat6.jpg", null));

                
                Album album2 = user.addAlbum("dogs");
                album2.addPhoto(Photo.createPhoto("stockphotos/dog1.jpg", null));
                album2.addPhoto(Photo.createPhoto("stockphotos/dog2.jpg", null));
                album2.addPhoto(Photo.createPhoto("stockphotos/dog3.jpg", null));
                album2.addPhoto(Photo.createPhoto("stockphotos/dog4.jpg", null));
                album2.addPhoto(Photo.createPhoto("stockphotos/dog5.jpg", null));
                album2.addPhoto(Photo.createPhoto("stockphotos/dog6.jpg", null));

            }
        }
        return model;
    }

    public static void storeModelToFile() {
		 if (model!=null) {
	        model.doCleanUp(true);
	         try {
	            FileOutputStream fileOut = new FileOutputStream("photo.dat");
	            ObjectOutputStream out = new ObjectOutputStream(fileOut);
	            out.writeObject(model);
	            out.close();
	            fileOut.close();
	         } catch (IOException i) {
	             i.printStackTrace();
	         }
		 }
	 }

    public void gotoAlbumFromUser() {
        albumStage.setScene(sceneAlbum);
        controllerAlbum.init();
        albumStage.setTitle("Album " + model.getCurUser().getCurrentAlbum().getAlbumName());
        albumStage.show();
    }

    public static void gotoAdminFromLogin() {
		loginStage.setScene(sceneAdmin);
		controllerAdmin.init();
		loginStage.setTitle("Welcome to the Admin Tool!");
		loginStage.show();
    }

    public static void gotoLoginFromAdmin() {
    	gotoLogin();
    }

    public static void gotoLoginFromAlbum() {
    	albumStage.hide();
    	gotoLogin();
    }

    public static void gotoLoginFromUser() {
    	albumStage.hide();
    	gotoLogin();
    }

    private static void gotoLogin() {
    	loginStage.setScene(sceneLogin);
		controllerLogin.init();
		loginStage.setTitle("Welcome To Photos App!");
		loginStage.show();
    }

    public static void gotoUserFromLogin() {
		loginStage.hide();
		gotoUser();
    }

    public static void gotoUserFromAdmin() {
		loginStage.hide();
		gotoUser();
    }

    public static void gotoUserFromAlbum() {
		gotoUser();
	}

    private static void gotoUser() {
		albumStage.setScene(sceneUser);
		controllerUser.init();
		albumStage.show();
	}
}
