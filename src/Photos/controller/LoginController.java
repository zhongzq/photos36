package Photos.controller;

import Photos.model.User;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Alert;

/** 
 * The first user interface is Login page which can be invoked when the the program is start.
 * 
 * 
 */

public class LoginController extends MainController implements IController {
  
	@FXML TextField userID;

    public void init() {
		getModel().setCurUser(null);
		userID.clear();
	}

    public void login() {
    	String user_id = userID.getText().trim();
    	User user = getModel().getUser(user_id);
    	if (user!=null) {
    		getModel().setCurUser(user);
        	if (user_id.equalsIgnoreCase("admin")) {
            	gotoAdminFromLogin();
        	} else {
            	gotoUserFromLogin();
        	}
    	} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setContentText("Username is not found");
			alert.showAndWait();
    		userID.setText("");
    	}
    }

    public void exit() {
		Platform.exit();
	}

}