package Photos.controller;

import Photos.model.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.time.LocalDate;
import java.util.List;

/** 
 * Non-Admin User Subsystem Once the Album is created by user, the initial information about the album should be displayed in the table.
 * Allow to Searching photo by Data Range/tags.
 * 

 */

public class UserController extends MainController implements EventHandler<MouseEvent>, IController, ChangeListener<Album> {

    @FXML TableView<Album> 	table;

    @FXML TableColumn 	colalbumName;
	@FXML TableColumn 	colphotoCount;
	@FXML TableColumn 	colstartTime;
	@FXML TableColumn 	colendTime;

	@FXML ListView<Tag> listTag;

    @FXML TextField tagName;
    @FXML TextField tagValue;
    @FXML TextField newAlbumName;

    @FXML DatePicker startDate;
    @FXML DatePicker endDate;


    /**
     * Initiating the User Subsystem to be shown
     */
    public void init() {
    	User user = getModel().getCurUser();
    	ObservableList<Album> albumList = user.getListAlbums();
    	listTag.setItems(user.getTags().getTags());
        if (user.getTags().getTags().size() > 0) {
        	listTag.getSelectionModel().select(0);
        }
    	for (Album a : albumList) {
    		a.setCounterDatetime();
    	}
    	table.setItems(albumList);
        if (user.getListAlbums().size() > 0) {
        	table.getSelectionModel().select(0);
        }
    	table.refresh();
	}

	
	public UserController() {}


    @FXML
    public void initialize() {
    	table.setEditable(true);
    	colalbumName.setCellFactory(TextFieldTableCell.<Album>forTableColumn());
    	colalbumName.setOnEditCommit(
            new EventHandler<CellEditEvent<Album, String>>() {
                @Override
                public void handle(CellEditEvent<Album, String> t) {
            		String newAlbumName = t.getNewValue().trim();
            		//
            		if (newAlbumName.length()>0) {
            			User user = getModel().getCurUser();
            			//
            			int i = table.getSelectionModel().getSelectedIndex();
            			//
            			user.updateAlbumName(i, newAlbumName);
            		}
            		//
        			table.refresh();
                }
            }
        );

    	table.setRowFactory(tableView -> {
            final TableRow<Album> row = new TableRow<>();
            final ContextMenu contextMenu = new ContextMenu();
            final MenuItem removeMenuItem = new MenuItem("Remove");
            removeMenuItem.setOnAction(event -> table.getItems().remove(row.getItem()));
            contextMenu.getItems().add(removeMenuItem);

            row.contextMenuProperty().bind(
                    Bindings.when(row.emptyProperty())
                    .then((ContextMenu)null)
                    .otherwise(contextMenu)
            );
            return row ;
        });

    	endDate.setValue(LocalDate.now());
    	startDate.setValue(endDate.getValue().minusDays(30));
    	table.getSelectionModel().selectedItemProperty().addListener(this);

    	table.setOnMouseClicked(event -> {
			if (!event.getButton().equals(MouseButton.PRIMARY) || event.getClickCount() != 1) {
				if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
                    gotoAlbumFromUser();
                }
			}
		});
    	
    }

    
    
	@Override
	public void handle(MouseEvent arg0) {}


    public void logOff() {
	    gotoLoginFromUser();
	}

    public void exit() {
		Platform.exit();
	}

    public void addTag() {
		String name = tagName.getText().trim();
		String value = tagValue.getText().trim();
        User user = getModel().getCurUser();
        if (name.length()>0 && value.length()>0) {
            if (user.getTags().addTag(name, value)) {
                listTag.refresh();
                tagName.setText("");
                tagValue.setText("");
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Duplicate Name and Value Pair!");
                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Name and Value Fields Cannot Be Empty!");
            alert.showAndWait();
        }
	}

    public void deleteTag() {
		User user = getModel().getCurUser();
		int index = listTag.getSelectionModel().getSelectedIndex();
		user.deleteTag(index);
		listTag.refresh();
	}

    public void searchDate() {
		User user = getModel().getCurUser();
		if (startDate.getValue() == null || endDate.getValue() == null) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Error");
            alert.setContentText("Please set search condition (dates) first!");
            alert.showAndWait();
		} else {
			user.getDates().setStartDate(startDate.getValue());
			user.getDates().setEndDate(endDate.getValue());
			List<Photo> list = user.searchByDate();
			if (list.size() > 0) {
				Album newAlbum = new Album("Search Result(Date)", list);
				newAlbum.setCounterDatetime();
				user.addOrOverwriteAlbum(newAlbum);
				
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	            alert.setTitle("Error");
	            alert.setContentText("Album 'Search Result(Date)' is created for search results. It will be replaced in next search.");
	            alert.showAndWait();

	            user.setCurrentAlbum(newAlbum);
	    		gotoAlbumFromUser();
			} else {
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	            alert.setTitle("Error");
	            alert.setContentText("No photo matched.");
	            alert.showAndWait();
			}
		}
	}

    public void searchTag() {
		User user = getModel().getCurUser();
		TagOperations tagCondition = user.getTags();
		if (tagCondition.getTags().size()==0) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Error");
            alert.setContentText("Please set search condition (tags) first.");
            alert.showAndWait();
		} else {
			List<Photo> lst = user.searchByTag();

			if (lst.size() > 0) {
				Album newOne = new Album("Search Result(Tag)", lst);
				newOne.setCounterDatetime();
				user.addOrOverwriteAlbum(newOne);
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	            alert.setTitle("Error");
	            alert.setContentText("Album 'Search Result(Tag)' is created for search results. It will be replaced in next search.");
	            alert.showAndWait();
			
			    user.setCurrentAlbum(newOne);
	    		gotoAlbumFromUser();
			} else {
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	            alert.setTitle("Error");
	            alert.setContentText("No photo matched.");
	            alert.showAndWait();
			}
		}
	}

    public void addNewAlbum() {
		User user = getModel().getCurUser();
		String album_name = newAlbumName.getText().trim();
		
		if (album_name.length() > 0) {
			if (user.addAlbum(album_name) != null) {
				user.addAlbum(album_name);
		    	table.refresh();
				newAlbumName.setText("");
			} else {
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	            alert.setTitle("Error");
	            alert.setContentText("Duplicated album name!");
	            alert.showAndWait();
			}
		}
	}

    @Override
	public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
    	PhotosModel model = getModel();
    	User user = model.getCurUser();
    	if (newValue!=null) {
    		user.setCurrentAlbum(newValue);
    	}
	}

}