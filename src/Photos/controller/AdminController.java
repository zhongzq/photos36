package Photos.controller;

import Photos.model.PhotosModel;
import Photos.model.User;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/** 
 * Admin Controller allow the admin to create and delete user account
 *  
 * 
 */

public class AdminController extends MainController implements IController, ChangeListener<User> {

    @FXML ListView<User> listView;
    @FXML TextField name;

    public AdminController() {}

    public void init() {
        name.clear();
	}

    public void initialize() {
    	PhotosModel model = getModel();
        listView.setItems(model.getUserList());
        listView.getSelectionModel().selectedItemProperty().addListener(this);
        if (model.getUserList().size() > 0) {
            listView.getSelectionModel().select(0);
        }
    }

    public void deleteUser() {
    	PhotosModel model = getModel();
    	int index = listView.getSelectionModel().getSelectedIndex();
    	if (index>=0) {
    		model.deleteUser(index);
    		listView.refresh();
    	}
    }

    public void addUser() {
    	String username = name.getText().trim();
    	PhotosModel model = getModel();
        if (!username.isEmpty()) {
        	User user = model.getUser(username);
            if (user!=null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("This name has already existed!");
                alert.showAndWait();
            } else {
                model.addUser(username);
                name.clear();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter a valid username!");
            alert.showAndWait();
        }
    }

    //jump instruction 
    public void logOff() {
	    gotoLoginFromAdmin();
	}

    public void exit() {
		Platform.exit();
	}

	@Override
	public void changed(ObservableValue<? extends User> observable, User oldValue, User newValue) {
		// TODO Auto-generated method stub
		
	}

}
